--[[
 Copyright 2019 Konsulko Group

 author:Stoyan Bogdanov <stoyan.bogdanov@konsulko.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
--]]


-- Test subscribe  [working ok]
_AFT.testVerbStatusSuccess('testSubscribeASuccess','network-manager','subscribe', {value="global_state"})
_AFT.testVerbStatusSuccess('testSubscribeBSuccess','network-manager','subscribe', {value="technologies"})
_AFT.testVerbStatusSuccess('testSubscribeCSuccess','network-manager','subscribe', {value="technology_properties"})
_AFT.testVerbStatusSuccess('testSubscribeDSuccess','network-manager','subscribe', {value="services"})
_AFT.testVerbStatusSuccess('testSubscribeESuccess','network-manager','subscribe', {value="service_properties"})
_AFT.testVerbStatusSuccess('testSubscribeFSuccess','network-manager','subscribe', {value="agent"})

-- Test unsubscribe
_AFT.testVerbStatusSuccess('testUnsubscribeASuccess','network-manager','unsubscribe', {value="global_state"})
_AFT.testVerbStatusSuccess('testUnsubscribeBSuccess','network-manager','unsubscribe', {value="technologies"})
_AFT.testVerbStatusSuccess('testUnsubscribeCSuccess','network-manager','unsubscribe', {value="technology_properties"})
_AFT.testVerbStatusSuccess('testUnsubscribeDSuccess','network-manager','unsubscribe', {value="services"})
_AFT.testVerbStatusSuccess('testUnsubscribeESuccess','network-manager','unsubscribe', {value="service_properties"})
_AFT.testVerbStatusSuccess('testUnsubscribeFSuccess','network-manager','unsubscribe', {value="agent"})

_AFT.testVerbStatusSuccess('testStateSuccess','network-manager','state', {})
_AFT.testVerbStatusSuccess('testOfflineSuccess','network-manager','offline', {})
_AFT.testVerbStatusSuccess('testTechnologiesSuccess','network-manager','technologies', {})
_AFT.testVerbStatusSuccess('testServicesSuccess','network-manager','services', {})

_AFT.testVerbStatusSuccess('testDisableTechnologySuccess','network-manager','disable_technology', {technology="ethernet"})
_AFT.testVerbStatusSuccess('testEnableTechnologySuccess','network-manager', 'enable_technology', {technology="ethernet"})

_AFT.testVerbStatusSuccess('testDisableTechnologySuccess','network-manager','disable_technology', {technology="wifi"})
_AFT.testVerbStatusSuccess('testEnableTechnologySuccess','network-manager', 'enable_technology', {technology="wifi"})

_AFT.testVerbStatusSuccess('testDisableTechnologySuccess','network-manager','disable_technology', {technology="bluetooth"})
_AFT.testVerbStatusSuccess('testEnableTechnologySuccess','network-manager', 'enable_technology', {technology="bluetooth"})


-- Test get_property
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','get_property', {technology="wifi"})
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','get_property', {technology="ethernet"})
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','get_property', {technology="bluetooth"})

-- Test set_property
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','set_property', {technology="ethernet",tethering="true"}
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','set_property', {technology="ethernet",tethering="false"}
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','set_property', {technology="wifi",tethering="true"}
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','set_property', {technology="wifi",tethering="false"}
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','set_property', {technology="bluetooth",tethering="true"}
_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','set_property', {technology="bluetooth",tethering="false"}

_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','scan_services', {technology="wifi"})

--_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','connect_service', {service="wifi_ec086b181ee3_436861726c6f74746520262043686c6f65_managed_psk"}

--_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','disconnect_service', {service="wifi_ec086b181ee3_436861726c6f74746520262043686c6f65_managed_psk"}

--_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','remove_service', {service="wifi_ec086b181ee3_436861726c6f74746520262043686c6f65_managed_psk"}

--_AFT.testVerbStatusSuccess('testgetpropertySuccess','network-manager','agent_response', {passphrase="hunter2", id=1}
