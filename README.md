# Network Service

## Overview

Network service uses the respective ConnMan package to administer networking interfaces

## Verbs

| Name               | Description                                             | JSON Response                                                                              |
|--------------------|---------------------------------------------------------|--------------------------------------------------------------------------------------------|
| subscribe          | subscribe to network events                             | *Request:* {"value": "global_state"}                                                       |
| unsubscribe        | unsubscribe to network events                           | *Request:* {"value": "global_state"}                                                       |
| state              | get current global network connection state             | {"response":"online"}                                                                      |
| offline            | set global offline mode                                 | *Request:* {"value": true}                                                                 |
| technologies       | returns the respective available technology types       | see technologies verb section                                                              |
| get_property       | get properties for a respective service/technology      | see get_property section                                                                   |
| set_property       | set property value for a respective service/technology  | see set_property section                                                                   |
| services           | returns services provided from technologies             | see services verb section                                                                  |
| enable_technology  | enable respective technology                            | *Request:* {"technology": "wifi"}                                                          |
| disable_technology | disable respective technology                           | *Request:* {"technology": "wifi"}                                                          |
| scan_services      | start scan on respective technology (if supported)      | *Request:* {"technology": "wifi"}                                                          |
| remove_service     | remove respective service                               | *Request:* {"service": "wifi_ec086b181ee3_436861726c6f74746520262043686c6f65_managed_psk"} |
| connect_service    | connect to respective service                           | *Request:* {"service": "wifi_ec086b181ee3_436861726c6f74746520262043686c6f65_managed_psk"} |
| disconnect_service | disconnect respective service                           | *Request:* {"service": "wifi_ec086b181ee3_436861726c6f74746520262043686c6f65_managed_psk"} |
| agent_response     | response information to confirm device association      | see agent_response verb section                                                            |

### technologies Verb

<pre>
 {
  "response": {
    "values": [
      {
        "technology": "p2p",
        "properties": {
          "name": "P2P",
          "type": "p2p",
          "powered": false,
          "connected": false,
          "tethering": false
        }
      },
      {
        "technology": "wifi",
        "properties": {
          "name": "WiFi",
          "type": "wifi",
          "powered": true,
          "connected": false,
          "tethering": false
        }
      },
      {
        "technology": "ethernet",
        "properties": {
          "name": "Wired",
          "type": "ethernet",
          "powered": true,
          "connected": true,
          "tethering": false
        }
      },
      {
        "technology": "bluetooth",
        "properties": {
          "name": "Bluetooth",
          "type": "bluetooth",
          "powered": true,
          "connected": false,
          "tethering": false
        }
      },
        "technology": "cellular",
        "properties": {
          "name": "Cellular",
          "type": "cellular",
          "powered": true,
          "connected": true,
          "tethering": false
        }
      }
    ]
  },
  ...
 }
</pre>

# get_property Verb

Request for a techology's properties:

<pre>
{
  "technology": "bluetooth",
}
</pre>

Response:

<pre>
{
  "response": {
    "name": "Bluetooth",
    "type": "bluetooth",
    "powered": true,
    "connected": false,
    "tethering": true
  },
  ...
}
</pre>

# set_property Verb

Request for setting properties for a technology:

<pre>
{
  "technology": "bluetooth",
  "properties": {
    "tethering": true
  }
}
</pre>

### services Verb

<pre>
{
  "response": {
    "values": [
     {
        "service": "cellular_310260157754769_context1",
        "properties": {
          "type": "cellular",
          "security": [],
          "state": "online",
          "strength": 100,
          "favorite": true,
          "immutable": false,
          "autoconnect": true,
          "name": "T-Mobile",
          "roaming": false,
          "ethernet": {
            "method": "auto",
            "interface": "wwp0s20u1u4u3i8",
            "address": "E2:7D:ED:EC:C2:33",
            "mtu": 1500
          },
          "ipv4": {
            "method": "fixed",
            "address": "100.194.51.219",
            "netmask": "255.255.255.248",
            "gateway": "100.194.51.220"
          },
          "ipv4.configuration": {
            "method": "fixed",
            "address": "100.194.51.219",
            "netmask": "255.255.255.248",
            "gateway": "100.194.51.220"
          },
          "ipv6": {},
          "ipv6.configuration": {
            "method": "off"
          },
          "nameservers": [
            "10.177.0.210"
          ],
          "nameservers.configuration": [],
          "timeservers": [
            "100.194.51.220",
            "pool.ntp.org"
          ],
          "timeservers.configuration": [],
          "domains": [],
          "domains.configuration": [],
          "proxy": {
            "method": "direct"
          },
          "proxy.configuration": {},
          "provider": {}
        }
      },
      {
        "service": "ethernet_0800271b86ef_cable",
        "properties": {
          "type": "ethernet",
          "security": [],
          "state": "ready",
          "favorite": true,
          "immutable": false,
          "autoconnect": true,
          "name": "Wired",
          "ethernet": {
            "method": "auto",
            "interface": "enp0s3",
            "address": "08:00:27:1B:86:EF",
            "mtu": 1500
          },
          "ipv4": {
            "method": "dhcp",
            "address": "192.168.56.3",
            "netmask": "255.255.255.0"
          },
          "ipv4.configuration": {
            "method": "dhcp"
          },
          "ipv6": {},
          "ipv6.configuration": {
            "method": "auto",
            "privacy": "disabled"
          },
          "nameservers": [],
          "nameservers.configuration": [],
          "timeservers": [
            "pool.ntp.org"
          ],
          "timeservers.configuration": [],
          "domains": [],
          "domains.configuration": [],
          "proxy": {
            "method": "direct"
          },
          "proxy.configuration": {},
          "provider": {}
        }
      },
      {
        "service": "wifi_ec086b181ee3_53494e4754454c2d35333630_managed_psk",
        "properties": {
          "type": "wifi",
          "security": [
            "psk"
          ],
          "state": "idle",
          "strength": 58,
          "favorite": false,
          "immutable": false,
          "autoconnect": false,
          "name": "SINGTEL-5360",
          "ethernet": {
            "method": "auto",
            "interface": "wlp0s12u3",
            "address": "EC:08:6B:18:1E:E3",
            "mtu": 1500
          },
          "ipv4": {},
          "ipv4.configuration": {
            "method": "dhcp"
          },
          "ipv6": {},
          "ipv6.configuration": {
            "method": "auto",
            "privacy": "disabled"
          },
          "nameservers": [],
          "nameservers.configuration": [],
          "timeservers": [],
          "timeservers.configuration": [],
          "domains": [],
          "domains.configuration": [],
          "proxy": {},
          "proxy.configuration": {},
          "provider": {}
        }
      },
      {
        "service": "wifi_ec086b181ee3_436861726c6f74746520262043686c6f65_managed_psk",
        "properties": {
          "type": "wifi",
          "security": [
            "psk",
            "wps"
          ],
          "state": "idle",
          "strength": 36,
          "favorite": false,
          "immutable": false,
          "autoconnect": false,
          "name": "Charlotte & Chloe",
          "ethernet": {
            "method": "auto",
            "interface": "wlp0s12u3",
            "address": "EC:08:6B:18:1E:E3",
            "mtu": 1500
          },
          "ipv4": {},
          "ipv4.configuration": {
            "method": "dhcp"
          },
          "ipv6": {},
          "ipv6.configuration": {
            "method": "auto",
            "privacy": "disabled"
          },
          "nameservers": [],
          "nameservers.configuration": [],
          "timeservers": [],
          "timeservers.configuration": [],
          "domains": [],
          "domains.configuration": [],
          "proxy": {},
          "proxy.configuration": {},
          "provider": {}
        }
      },
      {
        "service": "wifi_ec086b181ee3_42656c6c6128477565737429_managed_psk",
        "properties": {
          "type": "wifi",
          "security": [
            "psk"
          ],
          "state": "idle",
          "strength": 31,
          "favorite": false,
          "immutable": false,
          "autoconnect": false,
          "name": "Coffee Shop (Guest)",
          "ethernet": {
            "method": "auto",
            "interface": "wlp0s12u3",
            "address": "EC:08:6B:18:1E:E3",
            "mtu": 1500
          },
          "ipv4": {},
          "ipv4.configuration": {
            "method": "dhcp"
          },
          "ipv6": {},
          "ipv6.configuration": {
            "method": "auto",
            "privacy": "disabled"
          },
          "nameservers": [],
          "nameservers.configuration": [],
          "timeservers": [],
          "timeservers.configuration": [],
          "domains": [],
          "domains.configuration": [],
          "proxy": {},
          "proxy.configuration": {},
          "provider": {}
        }
      },
      {
        "service": "bluetooth_6030D46655A7_D0817A5ABC5E",
        "properties": {
          "type": "bluetooth",
          "security": [],
          "state": "idle",
          "favorite": false,
          "immutable": false,
          "autoconnect": false,
          "name": "Matt's iPhone",
          "ethernet": {
            "method": "auto"
          },
          "ipv4": {},
          "ipv4.configuration": {
            "method": "dhcp"
          },
          "ipv6": {},
          "ipv6.configuration": {
            "method": "auto",
            "privacy": "disabled"
          },
          "nameservers": [],
          "nameservers.configuration": [],
          "timeservers": [],
          "timeservers.configuration": [],
          "domains": [],
          "domains.configuration": [],
          "proxy": {},
          "proxy.configuration": {},
          "provider": {}
        }
      }
    ]
  },
  ...
}
</pre>

### agent_response Verb

After receiving an *agent event* the client must send back important information
to the service, which is usually a passphase and respective id.

<pre>
{
  "fields": {
    "passphrase": "hunter2"
  },
  "id": 1
}
</pre>

## Events

### technologies Event

<pre>
{
  "technology": "wifi",
  "action": "added",
  "properties": {
    "name": "WiFi",
    "type": "wifi",
    "powered": false,
    "connected": false,
    "tethering": false
  }
}
</pre>

### technology_properties Event

<pre>
{
  "technology": "wifi",
  "properties": {
    "powered": true
  }
}
</pre>

### services Event

<pre>
{
  "values": [
   {
     "service": "ethernet_080027ccbf98_cable",
     "action": "unchanged"
   },
   {
     "service": "ethernet_0800271b86ef_cable",
     "action": "unchanged"
   },
   {
     "service": "wifi_ec086b181ee3_415355535f4d31_managed_psk",
     "action": "changed",
     "properties": {
       "type": "wifi",
       "security": [
          "psk",
          "wps"
       ],
       "state": "idle",
       "strength": 95,
       "favorite": false,
       "immutable": false,
       "autoconnect": false,
       "name": "ASUS_M1",
       "ethernet": {
         "method": "auto",
         "interface": "wlan0",
         "address": "EC:08:6B:18:1E:E3",
         "mtu": 1500
       },
       "ipv4": {},
       "ipv4.configuration": {
         "method": "dhcp"
       },
       "ipv6": {},
       "ipv6.configuration": {
         "method": "auto",
         "privacy": "disabled"
       },
       "nameservers": [],
       "nameservers.configuration": [],
       "timeservers": [],
       "timeservers.configuration": [],
       "domains": [],
       "domains.configuration": [],
       "proxy": {},
       "proxy.configuration": {},
       "provider": {}
     }
   },
   ...
  ]
}
</pre>

### service_properties Event

<pre>
{
  "service": "wifi_ec086b181ee3_415355535f4d31_managed_psk",
  "properties": {
    "strength": 94
  }
}
</pre>

### agent Event

<pre>
{
  "id": 1,
  "method": "request-input",
  "service": "wifi_ec086b181ee3_415355535f4d31_managed_psk",
  "fields": {
    "passphrase": {
      "type": "psk",
      "requirement": "mandatory"
    },
    "wps": {
      "type": "wpspin",
      "requirement": "alternate"
    }
  }
}
</pre>

